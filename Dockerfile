#imagen base para mi dockerfile
FROM php:7.4-fpm
#Set working directory(estableciendo mi directorio de trabajo)
WORKDIR /var/www
#Install dependencies php
RUN apt-get update && apt-get install -y \

    build-essential \

    mariadb-client \

    libpng-dev \

    libjpeg62-turbo-dev \

    libfreetype6-dev \

    locales \

    zip \

    jpegoptim optipng pngquant gifsicle \

    vim \

    unzip \

    git \

    curl \

    gnupg \

    python \

    supervisor \

    libzip-dev 
#Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
#Install extensions
RUN docker-php-ext-install pdo_mysql zip exif pcntl bcmath gd
#Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# NPM(gestión de paquetes por defecto para Node.js)
RUN curl -sL https://deb.nodesource.com/setup_11.x  | bash -
RUN apt-get -y install nodejs
# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www
#decimos a la imagen que se levante con ese usuario
USER www
CMD ["php-fpm"]
